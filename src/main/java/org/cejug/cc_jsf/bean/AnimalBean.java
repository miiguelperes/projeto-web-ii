package org.cejug.cc_jsf.bean;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.cejug.cc_jsf.dao.AnimalDAO;
import org.cejug.cc_jsf.pojo.Animal;

/**
 * ManegedBean para gerenciar as páginas de listagem e edição/cadastro de animal.
 * 
 */ 
//Mapeamento via annotation
@ManagedBean(name = "animalBean") // animalBean representará a classe AnimalBean, caso campo name nao fosse declarado a classe AnimalBean seria representado por seu proprio nome em minusculo (animalBean)
@RequestScoped //menor escorpo do JSF
public class AnimalBean {

	// Propriedades
	
	/**
	 * Instancia da classe Animal para acessar os atributos da classe
	 * Representa o animal selecionado atualmente para editar, deletar ou alterar
	 */ 
	private Animal animalSelecionado = new Animal(); 

	/**
	 * instancia da classe AnimalDAO para salvar,alterar,deletar e listar os objetos	 * 
	 */ 
	private final AnimalDAO animalDAO = new AnimalDAO();

	/**
	 * Lista de animals, usada para exibir a listagem no dataTable do JSF 
	 */ 
	private List<Animal> lista = null; 

	// Constantes
	
	/**
	 * Redirecionamento para a página listagem e cadastro, usando constantes para facilitar a manutenção
	 */ 
	private final String PAGINA_LISTAGEM = "listagem?faces-redirect=true";
	private final String PAGINA_CADASTRO = "cadastro"; 

	// Construtor Padrão sem argumentos

	public AnimalBean() {
	}

	// Métodos

	/**
	 * Método responsável por salvar (caso seja um novo registro) ou alterar (caso o registro já exista na base) um animal
	 */ 
	public String salvar() {
		animalDAO.salvarOuAlterar(animalSelecionado); //usa o  método salvarOuAlterar da classe animalDAO para salvar o animal selecionado
		return PAGINA_LISTAGEM; //após de salvar, retorna a página de listagem dos animals
	}

	public String inserir() {	//método inserir o animal na pagina cadastro
		animalSelecionado = new Animal(); //instancia a classe animal para cadastro animal
		return PAGINA_CADASTRO; //retorna a página de cadastro/alteração do animal
	}
	
	public String editar() { //método para editar o animal cadastrado		
		return PAGINA_CADASTRO; //retorna a página de cadastro/alteração do animal
	}

	public String cancelar() { //metodo para cancelar o cadastramento do animal
		return PAGINA_LISTAGEM; //não salvar nada e retorna para a página de listagem dos animals
	}	
	
	public String deletar(){ //metodo para deletar o animal
		animalDAO.deletar(animalSelecionado); //chama o metodo deletar da classe animalDAO para deletar o animal selecionado
		return PAGINA_LISTAGEM; //retorna a pagina listagem dos animals
	}

	// Getters e Setters
	// Os get e set abaixo são necessários para que o JSF consiga pegar os valores do maneged bean e carregar na página e vice versa

	/**
	 * Retorna a lista de animals que será exibida no data table, caso a lista esteja null é carregada 
	 * através do método getTodos da classe AnimalDAO
	 */ 
	public List<Animal> getLista() {
		if (lista == null) {
			lista = animalDAO.getTodos();
		}
		return lista;
	}
	
	public void setLista(final List<Animal> lista) {
		this.lista = lista;
	}

	public Animal getAnimalSelecionado() {
		return animalSelecionado;
	}

	public void setAnimalSelecionado(final Animal animalSelecionado) {
		this.animalSelecionado = animalSelecionado;
	}
}

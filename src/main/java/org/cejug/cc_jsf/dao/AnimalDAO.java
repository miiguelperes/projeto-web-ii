package org.cejug.cc_jsf.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.cejug.cc_jsf.pojo.Animal;

/**
 * Classe responsável por fazer as operações básicas de CRUD com a classe animal
 *
 */
public class AnimalDAO {
	
	/**
	 * O EntityManager é uma API que nos da a possibilidade de realizar as operações de salvar, deletar, e prover
	 * meios de consulta para gerenciar uma entidade (ex: neste caso a entidade é o animal).
	 * 
	 */
	private EntityManager entityManager = null;
	
	//Construtor
	
	/**
	 * Ao instanciar a classe AnimalDAO o EntityManager é instanciado.
	 * A classe EntityManagerUtil retorna uma fábrica de entity manager que é o responsável por criar 
	 * instancias de EntityManagar, veja a classe EntityManagerUtil.
	 * 
	 */
	public AnimalDAO() { 
		entityManager = EntityManagerUtil.getEntityManagerFacotory().createEntityManager(); 
	}
	
	/**
	 * Salvar ou alterar um animal
	 * 
	 * @param animal
	 */
	public void salvarOuAlterar(Animal animal) { 
		entityManager.getTransaction().begin(); //inicia uma transação com o banco
		if (animal.getData() == null) { //caso o getDataDeCadastro seja null, retorna a data atual
			animal.setData(new Date());
			entityManager.persist(animal); //inserir os animals declarado nos campos
		}else{
			entityManager.merge(animal); //devolve o objeto para ser preenchido
		}
		entityManager.getTransaction().commit(); //retorna a transação e faz o commit para o banco
	}

	/**
	 * Deletar um animal
	 * 
	 * @param animal
	 */
	public void deletar(Animal animal) { 
		entityManager.getTransaction().begin(); //inicia uma transação com o banco
		entityManager.remove(animal); //remove o animal
		entityManager.getTransaction().commit(); //retorna a transação e faz o commit para o banco
	}

	/**
	 * Retorna uma lista com todos animals cadastrados
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Animal> getTodos() { 
		Query query = entityManager.createQuery("SELECT a FROM Animal a"); //retorna todos os animais do banco utilizando a lingaugem JPQL de consulta do JPA
		return query.getResultList(); //mostra o resultado da lista de animals cadastro no banco
	} 

}

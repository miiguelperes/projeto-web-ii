package org.cejug.cc_jsf.pojo;

import java.util.Date;
import java.io.Serializable;

//Importações necessárias para uso das anotações
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Classe que representa um animal
 *
 */
@Entity //define que esta classe será uma tabela em um base de dados
@Table(name = "animal") //define o nome da tabela na base de dados
public class Animal implements Serializable{

	//Propriedades

	private static final long serialVersionUID = 8114001930137970115L;
	@Id //diz que a propriedade id deve ser o identificador desta tabela no banco 
	@GeneratedValue(strategy = GenerationType.AUTO) //forma como será gerado este identificador
	private Integer id; //código único identificador 
	
	@Column(length = 50) //tamanho do campo nome 
	private String nome; //uma descrição para o animal
	
	private Integer idade; 
	@Column(precision=25, scale=10) //definindo precisão e escala
	private String local; 
	@Temporal(TemporalType.DATE) //diz que esta propriedade será uma data
	@Column(name = "data", nullable = false, updatable = false) // define o nome da coluna; que não pode ser nula; e a propriedade não pode ser alterada
	private Date data;
	@Column(length = 50) //tamanho do campo nome 
	private String raca; 
	@Column(length = 50) //tamanho do campo nome 
	private String cor;
	@Column(length = 50) //tamanho do campo nome 
	private String genero; 
	@Column(length=1) //tamanho do campo
	private Byte situacao; //Guardara o estado do produnto sendo estes: novo, bom estado, mais ou menos e quebrado
	
	//Construtores
	
	/**
	 * Construtor padrão, ele é necessário (obrigatório) para o correto funcionamento do JPA
	 */
	public Animal(){}
	
	//Getters e Setters

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return this.idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getLocal() {
		return this.local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getRaca() {
		return this.raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}

	public String getCor() {
		return this.cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getGenero() {
		return this.genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Byte getSituacao() {
		return this.situacao;
	}

	public void setSituacao(Byte situacao) {
		this.situacao = situacao;
	}


	//Métodos Sobrescritos
	
	/**
	 * Sobrescreve hashCode e equals
	 * Mais a respeito em: http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Animal other = (Animal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() { 
		return "Id: " + this.id + " Nome: " + this.nome;
	}
	
}
